(ns com.nusairat.router
  (:require [compojure.core :refer :all]))

(defroutes endpoints
  (context "/" []

    ; Our health checks
    (GET "/" []
      { :status 200 :body "HELLO WORLD!"})))

(def api
  (-> endpoints))