(defproject io.byton.bespin/nightsisters "0.1.0-SNAPSHOT"
  :min-lein-version "2.0.0"
  :description "NightSisters download service for OTA."
  :scm         {:url "https:/"}
  :dependencies [ 
    ;; Base
    [org.clojure/clojure "1.9.0"]
    [org.clojure/core.async "0.4.474"]
    [org.clojure/tools.logging "0.4.1"]
    [org.slf4j/slf4j-log4j12 "1.7.25"]
    [log4j/log4j "1.2.17" :exclusions [javax.mail/mail
                                      javax.jms/jms
                                      com.sun.jmdk/jmxtools
                                      com.sun.jmx/jmxri]]

    ;; To make app calls
    [compojure "1.6.1"]
    [ring/ring-json "0.4.0"]
    [ring/ring-defaults "0.3.2"]
  ]
  ; Bit redundant
  :plugins [
            ; checks for outdated projets
            [lein-ancient "0.6.15"]
            [lein-ring "0.12.4"]
            [lein-environ "1.1.0"]
            [lein-pprint "1.2.0"]]
  :ring {
         :handler com.nusairat.router/api }
  :source-paths ["src" "test"]
  :resource-paths ["resources"]
  :profiles {
             :uberjar       { }
             :runtime       {}
             :profiles/dev  {}
             :profiles/test {}}
  )