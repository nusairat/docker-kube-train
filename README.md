# Run
How to run these samples

## Clojure App
`lein ring server-headless`

`http://localhost:3000`

## Go
`export GOPATH=~/go`
`cp server.go $GOPATH/src`
`go run server.go`

`http://localhost:8080`

### Building
`RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /go/bin/server`

`"/go/bin/server`